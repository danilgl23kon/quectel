import serial
import os
import time
from serial.tools import list_ports
import ussd
from quectel import Quectel
# from dotenv import load_dotenv

# Load environment variables from .env file
# load_dotenv()
# Find all Quectel M35A modules


def listen_sms(q: Quectel, device):
    count = 0
    while count < 3000:
        if q.get_sms(device) == "BAD":
            time.sleep(1)
            count += 1
        else:
            return 'STOP'


def get_list_sms_from_memory(q: Quectel, device):
    msgs = q.list_all_sms(device).split("\n+")
    print(msgs)
    for msg in msgs:
        if "CMGL:" not in msg:
            continue
        msg = msg.split("\n")
        info_service = msg[0].split(",")
        msg = q.decode_sms(msg[1].replace("\r", ""))
        print("________")
        print(f"Status: {info_service[1]}\n"
              f"Sender: {info_service[2]}\n"
              f"Alphabet: {info_service[3]}\n"
              f"Time: {info_service[4]}\n"
              f"SMS: {msg}")
        print("________")


q = Quectel()
devices = ["/dev/cu.usbmodem114103"]
# q.get_info_ports()
for device in devices:
    # listen_sms(q, device)
    q.listen_sms(device)
    # get_list_sms_from_memory(q, device)
    # q.get_phone_number(device)
    # q.send_ussd(device, ussd.GET_PHONE_MTS)
    # q.get_operator(device)
    # q.get_imei(device)
    # q.get_signal_quality(device)
input()

modules = []
for port in list_ports.comports():
    try:
        ser = serial.Serial(port.device, 115200, timeout=1)
        ser.write(b'AT+CMGL="ALL"\r\n') # проверяет смс
        # ser.write(b'AT+CPIN?\r\n')
        response = ser.read(1000).decode('utf-8')
        print(response + f" Device: {port.device}, port: {port}")
        # Print SMS messages
        if '+CMGL:' in response:
            messages = response.split('\n')
            print(messages)
            # Преобразование строки в байты
            byte_array = bytearray.fromhex(messages[2])

            # Декодирование байтов с использованием кодировки UTF-16
            decoded_string = byte_array.decode('utf-16be')

            print(decoded_string)
        # if 'Quectel' in response and 'M35A' in response:
        modules.append(port.device)
        # ser.write(b'AT+CMGF=1\r\n')
        # response = ser.read(1000).decode('utf-8')
        # print(response)
        # print(f"Phone {response.strip()} is connected.")
        # ser.close()
    except Exception as e:
        print("Error decode: ", e)
        pass

# Set up a list of pins
pins = [os.environ.get(f'SIM{i+1}') for i in range(len(modules))]

# Set up serial connections for all modules
serials = [serial.Serial(port, 115200, timeout=1) for port in modules]

# Send PIN for each module
for i, pin in enumerate(pins):
    cmd = f'AT+CPIN="{pin}"\r\n'.encode('utf-8')
    serials[i].write(cmd)
    response = serials[i].read(1000).decode('utf-8')
    if 'OK' in response:
        print(f'Module {i+1}: PIN validated.')
    else:
        print(f'Module {i+1}: PIN validation failed.')
        continue

# Continuously read SMS messages for all modules
while True:
    for i, ser in enumerate(serials):
        # Check for new SMS messages
        ser.write(b'AT+CMGL="ALL"\r\n')
        response = ser.read(1000).decode('utf-8')

        # Print SMS messages
        if '+CMGL:' in response:
            messages = response.split('+CMGL:')
            for message in messages[1:]:
                print(f'Module {i+1}: {message.strip()}')

        # Check if module is ready to receive SMS
        ser.write(b'AT+CPMS?\r\n')
        response = ser.read(1000).decode('utf-8')
        if '+CPMS:' in response:
            message_info = response.split('+CPMS: ')[1].split(',')
            message_count = int(message_info[0]) - int(message_info[1])
            print(f'Module {i+1}: {message_count} new message(s) ready to read.')

    # Wait for a few seconds before checking again
    time.sleep(5)

# Close serial connections for all modules
for ser in serials:
    ser.close()