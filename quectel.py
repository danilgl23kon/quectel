import serial
import time
from serial.tools import list_ports


class Quectel:
    def __init__(self):
        self.devices = []
        self.operators = {}
        self.imeis = {}

    def get_info_ports(self):
        for port in list_ports.comports():
            try:
                ser = serial.Serial(port.device, 115200, timeout=1)
                ser.write(b'AT+CPIN?\r\n')
                response = ser.read(1000).decode('utf-8')
                ser.close()
                if "READY" in response:
                    print(f"SIM-карта подключена к устройству: {port.device}")
                    self.devices.append(port.device)
                else:
                    print(f"SIM-карта не найдена или заблокирована на устройстве: {port.device}")
            except Exception as e:
                print(f"Не удалось подключиться к порту {port.device}: {e}")

    def read_sms_by_index(self, ser, index):
        try:
            ser.write(f'AT+CMGR={index}\r\n'.encode())
            time.sleep(0.5)
            response = ser.read(ser.inWaiting()).decode('utf-8')
            # print(f"Full SMS from index {index}: {response}")
            messages = response.split('\n')
            # Преобразование строки в байты
            decoded_string = self.decode_sms(messages[2])
            print(f"Full SMS from index {index}: {decoded_string}")
        except Exception as e:
            print(f"Error reading SMS from index {index}: {e}")

    def decode_sms(self, response: str):
        try:
            # Преобразование строки в байты
            byte_array = bytearray.fromhex(response)
            # Декодирование байтов с использованием кодировки UTF-16
            decoded_string = byte_array.decode('utf-16be')
            return decoded_string
        except Exception as e:
            print(f"Error reading SMS: {e}")

    def listen_sms(self, device):
        try:
            ser = serial.Serial(device, 115200, timeout=1)
            ser.write(b'AT+CMGF=1\r\n')  # Установить текстовый режим SMS
            time.sleep(0.5)
            ser.write(b'AT+CNMI=2,1,0,0,0\r\n')  # Настроить уведомления о новых сообщениях
            time.sleep(0.5)

            print(f"Listening for SMS on {device}...")

            while True:
                if ser.inWaiting() > 0:
                    response = ser.read(ser.inWaiting()).decode('utf-8')
                    if "+CMTI:" in response:
                        print(f"Received SMS on {device}: {response}")
                        # Извлечь индекс сообщения
                        index = response.split(",")[1].strip()
                        self.read_sms_by_index(ser, index)
                time.sleep(0.5)
        except Exception as e:
            print(f"Error listening to device {device}: {e}")
        finally:
            ser.close()

    def list_all_sms(self, device):
        try:
            ser = serial.Serial(device, 115200, timeout=1)
            ser.write(b'AT+CMGF=1\r\n')  # Установить текстовый режим SMS
            time.sleep(0.5)
            ser.write(b'AT+CMGL="ALL"\r\n')  # Получить все сообщения
            time.sleep(1)
            print(ser.inWaiting())
            response = ser.readall().decode('utf-8')
            ser.close()
            print(response)
            if '+CMGL:' in response:
                messages = response
                print(f"Messages on {device}:")
                # for message in messages:
                #     print(message)
                return messages
            else:
                print(f"{device}: SMS не найдено")
                return []
        except Exception as e:
            print(f"Error listing SMS on device {device}: {e}")
            return []

    #Пока не работает
    def get_phone_number(self, device):
        try:
            ser = serial.Serial(device, 115200, timeout=1)
            ser.write(b'AT+CNUM\r\n')
            time.sleep(0.5)
            response = ser.readall().decode('utf-8').strip()
            ser.close()
            print(f"Raw response for AT+CNUM from device {device}: {response}")
            if "+CNUM" in response:
                lines = response.split('\n')
                for line in lines:
                    if "+CNUM" in line:
                        parts = line.split(',')
                        if len(parts) > 1:
                            phone_number = parts[1].strip('"')
                            print(f"Phone number on device {device}: {phone_number}")
                            return phone_number
            else:
                print(f"Не удалось получить номер телефона с устройства: {device}")
                return None
        except Exception as e:
            print(f"Error getting phone number from device {device}: {e}")
            return None

    def get_imei(self, device):
        try:
            ser = serial.Serial(device, 115200, timeout=1)
            ser.write(b'AT+CIMI\r\n')
            time.sleep(0.5)
            response = ser.read(ser.inWaiting()).decode('utf-8').strip()
            ser.close()
            print(f"IMEI on device {device}: {response}")
            imei = response.split("\n")
            self.imeis[device] = imei[1].replace("\r", "")
            return response
        except Exception as e:
            print(f"Error getting IMEI from device {device}: {e}")
            return None

    def send_ussd(self, device, ussd_code):
        try:
            ser = serial.Serial(device, 115200, timeout=1)
            ser.write(b'AT+CUSD=1,"' + ussd_code.encode() + b'",15\r\n')
            time.sleep(5)
            response = ser.read(ser.inWaiting()).decode('utf-8').strip()
            ser.close()
            msg = response.split(',')
            msg = msg[3].replace('"', "")
            print(f"USSD response on device {device}: {self.decode_sms(msg)}")
            return response
        except Exception as e:
            print(f"Error sending USSD code {ussd_code} on device {device}: {e}")
            return None

    def get_operator(self, device):
        try:
            ser = serial.Serial(device, 115200, timeout=1)
            ser.write(b'AT+COPS?\r\n')
            time.sleep(0.5)
            response = ser.read(ser.inWaiting()).decode('utf-8')
            ser.close()
            # print(f"Raw response for AT+COPS? from device {device}: {response}")
            if "+COPS:" in response:
                operator = response.split(",")[2].replace('\r\n\r\nOK\r\n', '').replace('"', "")
                print(f"Operator on device {device}: {operator}")
                self.operators[device] = operator
                return operator
            else:
                print(f"Не удалось получить информацию об операторе с устройства: {device}")
                return None
        except Exception as e:
            print(f"Error getting operator from device {device}: {e}")
            return None

    def listen_call(self, device):
        try:
            ser = serial.Serial(device, 115200, timeout=1)
            # Установить текстовый режим SMS (для получения читаемых сообщений от модема)
            ser.write(b'AT+CMGF=1\r\n')
            time.sleep(1)
            # ser.write(b'ATA\r\n')  # Команда для принятия входящего вызова
            # ser.write(b'AT+CREG?\r\n')
            time.sleep(1)

            while True:
                if ser.inWaiting() > 0:
                    response = ser.read(ser.inWaiting()).decode('utf-8')
                    print(f"Voice response: {response}")
                    break
                time.sleep(1)
        except Exception as e:
            print("Error listen call: ", e)
        finally:
            ser.close()

    def get_phone_calling(self, device):
        try:
            ser = serial.Serial(device, 115200, timeout=1)

            # Установка текстового режима SMS
            ser.write(b'AT+CMGF=1\r\n')
            time.sleep(1)

            # Включение функции CLI (Calling Line Identification)
            ser.write(b'AT+CLIP=1\r\n')
            time.sleep(1)

            print(f"Waiting for incoming call on {device}...")

            while True:
                if ser.inWaiting() > 0:
                    response = ser.read(ser.inWaiting()).decode('utf-8')
                    print(f"Received: {response}")

                    # Если в ответе содержится RING, значит приходит входящий вызов
                    if "RING" in response:
                        phone = response.split('"')[1]
                        return phone
                        # print("Incoming call detected.")
                        # # ser.write(b'ATA\r\n')  # Принять входящий вызов
                        # time.sleep(1)
                        # print("Call answered.")
                        # break
                time.sleep(1)
        except Exception as e:
            print(f"Error handling call on {device}: {e}")
        finally:
            ser.close()

    def get_signal_quality(self, device):
        try:
            ser = serial.Serial(device, 115200, timeout=1)

            # Отправляем команду AT+CSQ для запроса уровня сигнала
            ser.write(b'AT+CSQ\r\n')
            time.sleep(1)  # Ждем некоторое время для получения ответа

            response = ser.read(ser.inWaiting()).decode('utf-8')
            ser.close()
            # Парсим ответ для получения уровня сигнала
            if '+CSQ' in response:
                # Находим позицию после +CSQ:
                start_index = response.index('+CSQ:') + len('+CSQ:')
                # Выделяем числа после +CSQ:
                signal_info = response[start_index:].strip().split(',')[0]
                # Преобразуем в число
                signal_level = int(signal_info)
                return signal_level
            else:
                print(f"Error: Could not retrieve signal quality. Response: {response}")
                return None

        except Exception as e:
            print(f"Error communicating with {device}: {e}")
            return None